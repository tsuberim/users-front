import { gql, useMutation, useQuery } from '@apollo/client';
import React from 'react';
import { Segment, Container, Message, Button, Table, Form, Dropdown, Tab } from 'semantic-ui-react';
import { Formik } from 'formik';
import { BinatEnum, emptyUserInput, User, UserInput } from './api';

function UserRow({input, classifications, roles, onChange}: {input: UserInput, users: User[], classifications: BinatEnum[], roles: BinatEnum[], onChange: () => void}) {
  const [createUser, {error: createUserError}] = useMutation(gql`
    mutation CreateUser($user: String!, $name: String!, $classification: String!, $roles: [String!]!, $remark:String) {
      createUser(user:$user, name:$name, classification:$classification, roles:$roles, remark:$remark) {
        user
      }
    }`)

  const [updateUser, {error: updateUserError}] = useMutation(gql`
    mutation UpdateUser($user: String!, $name: String!, $classification: String!, $roles: [String!]!, $remark:String) {
      updateUser(user:$user, name:$name, classification:$classification, roles:$roles, remark:$remark) {
        user
      }
    }`)

  const [deleteUser, {error: deleteUserError, loading: deleteUserLoading}] = useMutation(gql`mutation DeleteUser($user:String!) {
    deleteUser(user:$user) {
      user
    }
  }`)

  return <Formik
  initialValues={input}
  validate={({ user, name, classification, roles }) => {
    const errors: any = {};
    if(!user) {
      errors.user = 'Required'
    }
    if(!name) {
      errors.name = 'Required'
    }
    if(!classification) {
      errors.classification = 'Required'
    }
    if(!roles) {
      errors.roles = 'Required'
    } else if(roles.length === 0) {
      errors.roles = 'Must select at least one role'
    }
    return errors;
  }}
  onSubmit={async (values, { setSubmitting, resetForm }) => {
    setSubmitting(true);
    if(input.user) {
      await updateUser({variables: values})
    } else {
      await createUser({variables: values})
    }
    await onChange();
    resetForm();
  }}
>
  {({
    values: { user, name, classification, roles: userRoles, remark },
    errors,
    touched,
    handleChange,
    setFieldValue,
    handleBlur,
    handleSubmit,
    isSubmitting,
    isValid
  }) => (
      <Table.Row>
        <Table.Cell width={3}>
          <Form.Input 
            required
            name='user' 
            placeholder='Username' 
            value={user} 
            error={touched.user && errors.user}
            onBlur={handleBlur}
            onChange={handleChange}
          />
        </Table.Cell>
        <Table.Cell width={3}>
          <Form.Input 
            required
            name='name' 
            placeholder='Name' 
            value={name} 
            error={touched.name && errors.name}
            onBlur={handleBlur}
            onChange={handleChange}
          />
        </Table.Cell>
        <Table.Cell width={3}>
          <Form.Dropdown 
            required
            name='classification' 
            placeholder='Classification' 
            selection 
            fluid 
            options={classifications.map(({literal, string}) => ({key: literal, text: string, value: literal}))}
            value={classification} 
            error={touched.classification && errors.classification}
            onChange={(e, data) => setFieldValue('classification', data.value)}
            onBlur={handleBlur}
          />
        </Table.Cell>
        <Table.Cell width={3}>
          <Form.Dropdown 
            required
            name='roles' 
            placeholder='Roles' 
            selection 
            multiple
            fluid 
            options={roles.map(({literal, string}) => ({key: literal, text: string, value: literal}))}
            value={userRoles} 
            error={touched.roles && errors.roles}
            onChange={(e, data) => setFieldValue('roles', data.value)}
            onBlur={handleBlur}
          />
        </Table.Cell>
        <Table.Cell width={2}>
          <Form.Input 
            name='remark' 
            placeholder='Remark' 
            value={remark} 
            error={touched.remark && errors.remark}
            onBlur={handleBlur}
            onChange={handleChange}
          />
        </Table.Cell>
        <Table.Cell width={5}>
          <Button circular primary error={createUserError || updateUserError} disabled={!isValid || isSubmitting} loading={isSubmitting} icon='save' onClick={() => handleSubmit()}/>
          {input.user && <Button circular color='red' error={deleteUserError} disabled={deleteUserLoading} loading={deleteUserLoading} icon='trash' onClick={async () => {
            if(window.confirm('האם את\ה בטוח?')) {
              await deleteUser({variables: {user: input.user}})
              await onChange();
            }
          }}/>}
        </Table.Cell>
      </Table.Row>
    )}
  </Formik>
}

function App() {
  const { loading, error, data, refetch } = useQuery<{users: User[], roles: BinatEnum[], classifications: BinatEnum[]}>(gql`{
    classifications {
      literal
      string
    } 
    roles {
      literal
      string
    }
    users {
      user
      name
      classification {
        literal
        string
      }
      roles {
        literal
        string
      }
      remark
    }
  }`)

  return (
    <Segment vertical padded loading={loading}>
      <Container>
        {error && <Message error content={error.message}/>}
        {data && (()=>{
          const {users, roles, classifications} = data

          return <Tab menu={{secondary: true}} panes={[
            {menuItem: 'Users', render: () => <Users users={users} classifications={classifications} roles={roles} refetch={refetch}/>},
            {menuItem: 'Classifications', render: () => <Classifications classifications={classifications} refetch={refetch}/>},
            {menuItem: 'Roles', render: () => <Roles roles={roles} refetch={refetch}/>},
          ]}/>
        })()}
      </Container>
    </Segment>
  );
}

function Users({users, roles, classifications, refetch}: {users: User[], roles: BinatEnum[], classifications: BinatEnum[], refetch: () => void}) {
  return <Form>
    <Table>
      <Table.Header>
        <Table.HeaderCell>
          user
        </Table.HeaderCell>
        <Table.HeaderCell>
          name
        </Table.HeaderCell>
        <Table.HeaderCell>
          classification
        </Table.HeaderCell>
        <Table.HeaderCell>
          role
        </Table.HeaderCell>
        <Table.HeaderCell>
          remark
        </Table.HeaderCell>
        <Table.HeaderCell>
          
        </Table.HeaderCell>
      </Table.Header>
      <Table.Body>
        {!users.length && <Table.Row>
          <Table.Cell colspan={5} textAlign='center'>
            Nothing to show
          </Table.Cell>
        </Table.Row>}
        {users.map((user) => <UserRow 
          input={{...user, roles: user.roles.map(r => r.literal), classification: user.classification.literal}} 
          users={users} 
          classifications={classifications} 
          roles={roles}
          onChange={refetch}
        />)}
      </Table.Body>
      <Table.Footer>
        <UserRow 
          input={emptyUserInput} 
          users={users} 
          classifications={classifications} 
          roles={roles}
          onChange={refetch}
        />
      </Table.Footer>
    </Table>
  </Form>
}

function ClassificationRow({input, onChange}: {input: BinatEnum, onChange: () => void}) {
  const [createClassification, {error: createClassificationError, loading: createClassificationLoading}] = useMutation(gql`
    mutation CreateClassification($literal: String!, $string: String!) {
      createClassification(literal:$literal, string:$string) {
        literal
        string
      }
    }`)

  const [updateClassification, {error: updateClassificationError, loading: updateClassificationLoading}] = useMutation(gql`
    mutation UpdateClassification($literal: String!, $string: String!) {
      updateClassification(literal:$literal, string:$string) {
        literal
        string
      }
    }`)

  return <Formik
  initialValues={input}
  validate={({ literal, string }) => {
    const errors: any = {};
    if(!literal) {
      errors.literal = 'Required'
    }
    if(!string) {
      errors.string = 'Required'
    }
    return errors;
  }}
  onSubmit={async (values, { setSubmitting, resetForm }) => {
    setSubmitting(true);
    if(input.literal) {
      await updateClassification({variables: values})
    } else {
      await createClassification({variables: values})
    }
    await onChange();
    resetForm();
  }}
>
  {({
    values: { literal, string },
    errors,
    touched,
    handleChange,
    setFieldValue,
    handleBlur,
    handleSubmit,
    isSubmitting,
    isValid
  }) => (
      <Table.Row>
        <Table.Cell>
          {input.literal || <Form.Input 
            required
            name='literal' 
            placeholder='Literal' 
            value={literal} 
            error={touched.literal && errors.literal}
            onBlur={handleBlur}
            onChange={handleChange}
          />}
        </Table.Cell>
        <Table.Cell>
          <Form.Input 
            required
            name='string' 
            placeholder='string' 
            value={string} 
            error={touched.string && errors.string}
            onBlur={handleBlur}
            onChange={handleChange}
          />
        </Table.Cell>
        <Table.Cell>
          <Button circular primary error={createClassificationError || updateClassificationError} disabled={!isValid || isSubmitting} loading={isSubmitting} icon='save' onClick={() => handleSubmit()}/>
        </Table.Cell>
      </Table.Row>
    )}
  </Formik>
}

function Classifications({classifications, refetch}: {classifications: BinatEnum[], refetch: () => void}) {
  return <Form>
    <Table>
      <Table.Header>
        <Table.HeaderCell>
          Literal
        </Table.HeaderCell>
        <Table.HeaderCell>
          String
        </Table.HeaderCell>
        <Table.HeaderCell>
        </Table.HeaderCell>
      </Table.Header>
      <Table.Body>
        {!classifications.length && <Table.Row colspan={2}>Nothing to show</Table.Row>}
        {classifications.map(classification => <ClassificationRow input={classification} onChange={refetch}/>)}
      </Table.Body>
      <Table.Footer>
        <ClassificationRow input={{literal: '', string: ''} as BinatEnum} onChange={refetch}/>
      </Table.Footer>
    </Table>
  </Form>
}

function RoleRow({input, onChange}: {input: BinatEnum, onChange: () => void}) {
  const [createRole, {error: createRoleError, loading: createRoleLoading}] = useMutation(gql`
    mutation CreateRole($literal: String!, $string: String!) {
      createRole(literal:$literal, string:$string) {
        literal
        string
      }
    }`)

  const [updateRole, {error: updateRoleError, loading: updateRoleLoading}] = useMutation(gql`
    mutation UpdateRole($literal: String!, $string: String!) {
      updateRole(literal:$literal, string:$string) {
        literal
        string
      }
    }`)

  return <Formik
  initialValues={input}
  validate={({ literal, string }) => {
    const errors: any = {};
    if(!literal) {
      errors.literal = 'Required'
    }
    if(!string) {
      errors.string = 'Required'
    }
    return errors;
  }}
  onSubmit={async (values, { setSubmitting, resetForm }) => {
    setSubmitting(true);
    if(input.literal) {
      await updateRole({variables: values})
    } else {
      await createRole({variables: values})
    }
    await onChange();
    resetForm();
  }}
>
  {({
    values: { literal, string },
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
    isSubmitting,
    isValid
  }) => (
      <Table.Row>
        <Table.Cell>
          {input.literal || <Form.Input 
            required
            name='literal' 
            placeholder='Literal' 
            value={literal} 
            error={touched.literal && errors.literal}
            onBlur={handleBlur}
            onChange={handleChange}
          />}
        </Table.Cell>
        <Table.Cell>
          <Form.Input 
            required
            name='string' 
            placeholder='string' 
            value={string} 
            error={touched.string && errors.string}
            onBlur={handleBlur}
            onChange={handleChange}
          />
        </Table.Cell>
        <Table.Cell>
          <Button circular primary error={createRoleError || updateRoleError} disabled={!isValid || isSubmitting} loading={isSubmitting} icon='save' onClick={() => handleSubmit()}/>
        </Table.Cell>
      </Table.Row>
    )}
  </Formik>
}

function Roles({roles, refetch}: {roles: BinatEnum[], refetch: () => void}) {
  return <Form>
    <Table>
      <Table.Header>
        <Table.HeaderCell>
          Literal
        </Table.HeaderCell>
        <Table.HeaderCell>
          String
        </Table.HeaderCell>
        <Table.HeaderCell>
        </Table.HeaderCell>
      </Table.Header>
      <Table.Body>
        {!roles.length && <Table.Row colspan={2}>Nothing to show</Table.Row>}
        {roles.map(role => <RoleRow input={role} onChange={refetch}/>)}
      </Table.Body>
      <Table.Footer>
        <RoleRow input={{literal: '', string: ''} as BinatEnum} onChange={refetch}/>
      </Table.Footer>
    </Table>
  </Form>
}

export default App;
