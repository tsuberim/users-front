import { ApolloClient, gql, InMemoryCache } from "@apollo/client";

export const client = new ApolloClient({
    uri: 'http://localhost:3000/graphql', 
    cache: new InMemoryCache()
});

export interface BinatEnum {
    literal: string;
    string: string;
}

export interface User {
    user: string,
    name: string,
    classification: BinatEnum,
    roles: BinatEnum[],
    remark?: string
}

export interface UserInput {
    user?: string,
    name?: string,
    classification?: string,
    roles: string[],
    remark?: string
}

export const emptyUserInput: UserInput = {
    user: '',
    name: '',
    classification: '',
    roles: [],
    remark: ''
}